var mat = [
  4, 3, 2, 5, 6, 2, 3, 4,
  1, 1, 1, 1, 1, 1, 1, 1,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  1, 1, 1, 1, 1, 1, 1, 1,
  4, 3, 2, 5, 6, 2, 3, 4
];

var position = [0, 4, 1, 6, 2, 5, 3];

var nll = 0, tower = 0, pawn = 0, queen = 0, bishop = 0, king = 0, horse = 0;

for (var i = 0; i < mat.length; i++) {
  var item = mat[i];
  nll += check(item, position[0]);
  tower += check(item, position[1]);
  pawn += check(item, position[2]);
  queen += check(item, position[3]);
  bishop += check(item, position[4]);
  king += check(item, position[5]);
  horse += check(item, position[6]);
}

function check(listItem, type) {
  var check = listItem == type;
  return +check;
}

console.log('Peão: ' + pawn + ' peça(s)');
console.log('Bispo: ' + bishop + ' peça(s)');
console.log('Horse: ' + horse + ' peça(s)');
console.log('Tower: ' + tower + ' peça(s)');
console.log('Rainha: ' + queen + ' peça(s)');
console.log('Rei: ' + king + ' peça(s)');