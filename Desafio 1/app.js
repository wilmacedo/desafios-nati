var list = [];

function addCot(title) {
  list.push(title);
}

function removeCot(title) {
  var index = list.indexOf(title);

  if (index > -1) {
    list.splice(index, 1);
  } else {
    throw new ReferenceError('Elemento não encontrado na lista.')
  }
}

function changeTitle(title, nw) {
  var index = list.indexOf(title);

  if (index > -1) {
    list[index] = nw;
  } else {
    throw new ReferenceError('Elemento não encontrado na lista.')
  }
}