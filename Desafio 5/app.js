var Func = function (id, name, office) {
  this.id = id;
  this.name = name;
  this.office = office;
}

var cargos = [2500, 1500, 10000, 1200, 800];
var funcs = [
  new Func(15, 'Joao da Silva', 1),
  new Func(1, 'Pedro Santos', 2),
  new Func(26, 'Maria Oliveira', 3),
  new Func(12, 'Rita Alcantara', 5),
  new Func(8, 'Ligia Matos', 2)
];

function addOffice(value) {
  cargos.push(value);
}

function addFunc(id, name, office) {
  var func;

  for (func of funcs) {
    if (func.id == id) {
      console.log('Erro: Ja existe um funcionario cadastrado com este id.');
      return;
    }

    for (var i = 0; i < cargos.length; i++) {
      if (office > cargos.length || office < 0) {
        console.log('Erro: O cargo não existe.');
        return;
      }
    }
  }

  funcs.push(new Func(id, name, office));
}

function showReport() {
  var func;

  for (func of funcs) {
    console.log('Codigo: ' + func.id);
    console.log('Nome: ' + func.name);
    console.log('Cargo: ' + func.office);
  }
}

function getBalance(office) {
  var func, sum = 0;
  
  for (var i = 0; i < cargos.length; i++) {
    if (office - 1 > cargos.length || office - 1 < 0) {
      console.log('Erro: O cargo não existe.');
      return;
    }
  }

  for (func of funcs) {
    if (func.office == office) {
      sum += cargos[office - 1];
    }
  }
  
  return sum;
}